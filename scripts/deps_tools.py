#!/usr/bin/python

from collections import defaultdict
from taGrammar import TAGrammar
import sys
import os
from englishLemmatizer import EnglishLemmatizer

CONTROL_VERB_LIST = "../../data/controlverbs.list"

class DepLink:
    """
    A single treebank dependency link.
    """ 
    normalizeDict = {"-COMMA-":",",\
                     "-PERIOD-":".",\
                     "-LCB-":"(",\
                     "-RCB-":")",\
                     "_COLON_":":",\
                     "-COLON-":":"}
    
    def __init__(self):
        pass

    def __init__(self,treebankline, reNormalize = True):

        # Fix for artefacts in input (probably a Mica Bug?
        treebankline = treebankline.replace("(POS)","")
        terebankline = treebankline.replace("(GovPOS)","")

        
        parts = treebankline.strip().split("||")
        if len(parts) == 2:
            structure, features = parts
        else: 
            structure = treebankline.strip() 
            features = "" 
        itemlist = structure.strip().split()
        self.attributes = {}
       
        if len(itemlist)<9:  # Fix for tokens without parents (how can that happen)
            self.node_id     = int(itemlist[0])
            self.word        = itemlist[1].strip()
            if reNormalize: 
                if self.word in self.normalizeDict:
                    self.word = self.normalizeDict[self.word]
            self.pos         = itemlist[2]
            self.parent_id   = self.node_id 
            self.tree        = itemlist[4]
            self.parent_tree = itemlist[5]
            self.difference  = itemlist[6]
            self.target_node = itemlist[7] # This is undocumented!      
            self.parent_pos = ""
            self.parent_word= ""
            self.attributes["disconnect"]="+"
        
        else:
            self.node_id     = int(itemlist[0])
            self.word        = itemlist[1].strip()
            if reNormalize: 
                if self.word in self.normalizeDict:
                    self.word = self.normalizeDict[self.word]
            self.pos         = itemlist[2]
            self.pos         = itemlist[2]
            self.parent_id   = int(itemlist[3])
            self.parent_word = itemlist[4]
            if reNormalize: 
                if self.parent_word in self.normalizeDict:
                    self.parent_word = self.normalizeDict[self.parent_word]
            self.pos         = itemlist[2]
            self.parent_pos  = itemlist[5]
            self.difference  = itemlist[6]
            self.tree        = itemlist[7]
            self.parent_tree = itemlist[8]
            self.target_node = itemlist[9] # This is undocumented!       
        
        attribs = features.split()
        for x in attribs:
            attr, val = x.split(":")
            self.attributes[attr] = val

    def __str__(self):
        result =  str(self.node_id) + " " + self.word + " " + self.pos + " " + str(self.parent_id) + " "+self.parent_word+" "+self.parent_pos + " "+self.tree + " "+self.parent_tree +" "+str(self.target_node) + '||' 

        for att in self.attributes: 
            result += " "+att+":"+str(self.attributes[att])
        return result

               
                
class DepsFile:
    """ 
    An interface to a treebank file that provides an iterator over sentences and words in the sentence.
    """
    
    def __init__(self, aTreebankfile):
        """
        Create a new treebank interface wrapping a treebank file.
        """
        self.treebankfile = aTreebankfile
       
                
    def next_sentence(self):
        """
        Read in a sentence from the treebank and return a list of tokens.
        """
       
        parses = []
        
        line = self.treebankfile.readline()
        
        if not line:
            return False
        
        line = line.strip()
        
        #while not (line.startswith("...NEW...") or line.startswith("___NEW___")):
            
        nodes = {} 

        while not (line.startswith("...EOS...") or line.startswith("___EOS___")): # for each sentence
            if not line.startswith("#"):
                link = DepLink(line) # parse description for each dependency link
                nodes[int(link.node_id)] = link
            line = self.treebankfile.readline().strip()
        parses.append(nodes) 
        line = self.treebankfile.readline().strip() #skip second EOS marker                
        #line = self.treebankfile.readline().strip() #skip second EOS marker                

        #line = self.treebankfile.readline().strip() #skip second NEW marker
        
        return parses

    def sentence_iterator(self):
        sentence = self.next_sentence()
        while sentence:
            yield(sentence)
            sentence = self.next_sentence()        


def sort(adj1,adj2):
    return adj1.node_id - adj2.node_id
    
def merge_ads(rightadj, leftadj):
    rpos = 0 
    lpos = 0
    result = []    
    while rpos<len(rightadj) and lpos<len(leftadj):
        if rightadj[rpos].node_id<=leftadj[lpos].node_id:
            result.append(rightadj[rpos])
            rpos += 1
        else:
            result.append(leftadj[lpos])
            lpos += 1
    if lpos<len(leftadj):
        result += leftadj[lpos:]
    if rpos<len(rightadj):
        result += rightadj[rpos:]
    return result
    

def sort_adjs(grammar, adjunctions):
    leftadjoining = []
    rightadjoining = []

    for adj in adjunctions:
        if grammar[adj.tree].is_left_adjoining():
            leftadjoining.append(adj)
            adj.attributes["adir"] = "l"
        elif grammar[adj.tree].is_right_adjoining():
            rightadjoining.append(adj)
            adj.attributes["adir"] = "r"

    leftadjoining.sort(lambda a,b: int(a.node_id) - int(b.node_id))
    rightadjoining.sort(lambda a,b: int(b.node_id) - int(a.node_id))
    return merge_ads(leftadjoining, rightadjoining)
    

def remove_multiple_adjunctions_and_co_anchors(treebank, grammar):
            
    for sentence in treebank.sentence_iterator():
        ok = remove_multi_adj_in_sentence(sentence, grammar)
        ok = remove_co_anchors(ok, grammar)

        for link in ok:
            print link
        print "___EOS___" 
        print "___EOS___" 
  

def remove_co_anchors(sentence, grammar):
    seen = defaultdict(DepLink);

    for depLink in sentence: 
        seen[int(depLink.node_id)] = depLink

    for depLink in sentence:
        if depLink.tree == "tCO": 
            seen[int(depLink.node_id)] = None
            parent = seen[int(depLink.parent_id)]
            parent.word += "_"+depLink.word
            for d in seen:
                if seen[d] and seen[d].parent_id == depLink. parent_id:
                   seen[d].parent_word += "_"+depLink.word
                

    result = []
    keys = [x for x in seen.keys() if seen[x]]
    keys.sort()
    for pos in keys:
        link = seen[pos]        
        link.node_id = str(pos)
        result.append(link)

    return result


def remove_multi_adj_in_sentence(sentence, grammar):
    seen = defaultdict(list);
    
    ok = []
    for depLink in sentence:
        # Count all adjunctions
        if grammar[depLink.tree].tree_type == "aux":
            seen[(depLink.parent_id, depLink.parent_word, depLink.parent_tree, depLink.parent_pos, depLink.target_node)].append(depLink)
        else:
            ok.append(depLink)
     
    for x in seen:
        adjunctions = seen[x]
        
        adjunctions = sort_adjs(grammar, adjunctions)            
      
        parent_id, parent_word, parent_tree, parent_pos, target_node = x
        for adj in adjunctions: 
            adj.difference = str(int(parent_id) -int(adj.node_id))
            adj.parent_id, adj.parent_word, adj.parent_tree, adj.parent_pos, adj.target_node = \
                 parent_id, parent_word, parent_tree, parent_pos, target_node
            parent_id, parent_word, parent_tree, parent_pos = adj.node_id, adj.word, adj.tree, adj.pos         
            target_node = grammar[adj.tree].root_node.num
            ok.append(adj)
                   
    ok.sort(lambda x,y: int(x.node_id) - int(y.node_id))
    
    return ok



def check_multiple_adjunctions(treebank, grammar):
    
    for sentence in treebank.sentence_iterator():

        seen = defaultdict(list);
    
        ok = []

        for depLink in sentence:
            # Count all adjunctions
            if grammar[depLink.tree].tree_type == "aux":
                seen[(depLink.parent_id, depLink.parent_word, depLink.parent_tree, depLink.parent_pos, depLink.target_node)].append(depLink)
            else:
                ok.append(depLink)

        for x in seen:
            adjunctions = seen[x]
            leftadjbefore = []
            rightadjbefore = [] 
            leftadjafter = []
            rightadjafter = [] 

            for adj in adjunctions:
                #print adj.node_id, adj.parent_id, adj
                if int(adj.node_id) < int(adj.parent_id):
                    if grammar[adj.tree].is_left_adjoining():
                        leftadjbefore.append(adj.tree)
                    elif grammar[adj.tree].is_right_adjoining():
                        rightadjbefore.append(adj.tree)
                    else:
                        print "Found wrapping tree "+adj.tree+" in multiple adjunction."
                        return False
                else:
                    if grammar[adj.tree].is_left_adjoining():
                        leftadjafter.append(adj.tree)
                    elif grammar[adj.tree].is_right_adjoining():
                        rightadjafter.append(adj.tree)
                    else:
                        print "Found wrapping tree "+adj.tree+" in multiple adjunction."
                        return False
            if (leftadjbefore and rightadjbefore) or (leftadjafter and rightadjafter):
                print "incompatible adjunction directions"
                print adj
                print leftadjbefore, rightadjbefore, " ... ", leftadjafter, rightadjafter
                return False
            if leftadjbefore:
                print "Weird left: "
                print adj
            if rightadjafter:          
                print "Weird right: "
                print adj
    return True
 
def computeDerivationTree(sentence, grammar):
    return (derivation_tree, deriv_tree_node_to_lex_entry, root_node)

def addSpansToParse(sentence):
    current = 0
    for dep in sentence:
        sentence[dep].attributes["start"] = current
        sentence[dep].attributes["end"] =  current + len(sentence[dep].word)-1
        current = current + len(sentence[dep].word) + 1

class DerivationTree():
    
    def __init__(self, sentence):
        derivation_tree = {}
        deriv_tree_node_to_lex_entry = {}
        root_node = None

        addSpansToParse(sentence)

        for depLink in sentence.values():
            if not depLink.parent_id in derivation_tree:
                derivation_tree[depLink.parent_id] = {}
            if not depLink.node_id in derivation_tree:
                derivation_tree[depLink.node_id] = {}
            if depLink.node_id == depLink.parent_id:
                root_node = depLink.node_id
            else:
                if not depLink.target_node in derivation_tree[depLink.parent_id]:
                    derivation_tree[depLink.parent_id][depLink.target_node] = []
                derivation_tree[depLink.parent_id][depLink.target_node].append(depLink.node_id)
                deriv_tree_node_to_lex_entry[depLink.parent_id] = (depLink.parent_tree+"x"+depLink.parent_pos, depLink.parent_word)
            deriv_tree_node_to_lex_entry[depLink.node_id] = (depLink.tree+"x"+depLink.pos, depLink.word)
       
        self.derivation_tree = derivation_tree
        self.deriv_tree_node_to_lex_entry = deriv_tree_node_to_lex_entry
        self.root_node = root_node
        self.parse = sentence
   

    def attachRaisedSubjects(self):
        lemmatizer = EnglishLemmatizer.getSingleton()
        if not lemmatizer: 
            lemmatizer = EnglishLemmatizer()

        if not lemmatizer.hasWordList("controlVerbsSubject"):
            lemmatizer.loadControlWordList("controlVerbsSubject", "controlVerbsObject", CONTROL_VERB_LIST)

        stack = [self.root_node]

        lastCvSubject = None
        lastVerbSubject = None

        seen = []

        while stack:
            currentWord = stack.pop()

            children = []
            for nc in self.derivation_tree[int(currentWord)].values():
                children += [int(c) for c in nc]


            wordInfo = self.parse[int(currentWord)]
            if wordInfo.pos.startswith("V"):
                lemma = lemmatizer.process_word(wordInfo.word,"V")

                # If this verb has a covert subject, mark the subject of the 
                # closest control verb in the tree as its DSubj
                if "esubj" in wordInfo.attributes and wordInfo.attributes["esubj"]=="+":
                    if lastCvSubject:
                        wordInfo.attributes["DSubj"] = str(lastCvSubject)
                        self.derivation_tree[currentWord]["esubj"] = [lastCvSubject]
                    
                    #elif lastVerbSubject:
                    #    self.parse[currentWord].attributes["dsubj"] = str(lastVerbSubject)
                    #    wordInfo.attributes["DSubj"] = str(lastVerbSubject)
                        #self.derivation_tree[currentWord]["esubj"] = [lastVerbSubject]

                        

                # If this verb is a control verb and has a realized subject, store this 
                # subject so we can attach it to lower verbs without a subject
                for child in children: 
                    childInfo = self.parse[child]
                    if "DRole" in childInfo.attributes and childInfo.attributes["DRole"]=="0":
                        if lemmatizer.isInWordList("controlVerbsSubject",lemma):
                            lastCvSubject = child
                            children.remove(child) # Don't need to traverse the subject branch
                            break
                        else:   
                            lastVerbSubject = child
                    if "DRole" in childInfo.attributes and childInfo.attributes["DRole"]=="1":
                        if lemmatizer.isInWordList("controlVerbsObject",lemma):
                            lastCvSubject = child
                            children.remove(child) # Don't need to traverse the subject branch
                            break
                
            for child in children:
                if not child in seen:  
                    stack.append(child)
                    seen.append(child)

            

    def getPotentialFEEs(self):
        """
        Get a set of possible frame evoking elements in this parse
        (represented by node numbers). 
        """
        possiblefee = set()
        
        for node in self.derivation_tree:
            #if "DSub" in self.parse[node].attributes and \
            #    self.parse[node].attributes["DSub"] != "nil" \
            #    and self.parse[node].pos[0] in "VJN":
            if self.parse[node].pos[0] == "V":
                possiblefee.add(node)
        
        return possiblefee 
  
    def getYield(self, node):
        return self.getYieldAndSpan(node)[2]
    
    def getYieldAndSpan(self, node):
        children = [node]
        directchildren = []
        for dc in self.derivation_tree[node].values():
            for c in dc: 
                if self.parse[c].parent_id==node:
                    directchildren.append(c)
        for child in directchildren:
            s, e, c = self.getYieldAndSpan(child) 
            children += c
        children.sort()
        start = self.parse[children[0]].attributes["start"]
        end = self.parse[children[-1]].attributes["end"] 
        return (start, end, children)

    def assignPhraseType(self, node):
        """
        Assign a phrase type for this argument.
        """
        if "root" in self.parse[node].attributes:
            cat = self.parse[node].attributes["root"]
        else: 
            cat = "" 
        phrase = self.getYield(node)
        
        whwords = ["how","where","when", "what", "which", "who", "whom", "whose", "why"]

        if self.parse[phrase[0]].pos == "``":
            return "QUO"
        elif self.parse[phrase[0]].pos == "PRP$" or \
            "pos" in [self.parse[x].pos.lower() for x in phrase]:
                return "POSS"
        elif cat.startswith("NP"):
            return "NP"
        elif cat.startswith("N"):
            return "N"
        elif cat.startswith("S"):
            if self.parse[phrase[0]].word.lower() in ["whether", "if"]:
                return "Swhether"
            elif self.parse[phrase[0]].word.lower() in whwords:
                return "Sinterrog"
            elif len(phrase)>1 and  self.parse[phrase[1]].word.lower() in whwords:
                return "Sinterrog"
            elif self.parse[node].pos.lower() == "vbg":
                return "Sing"
            elif len(phrase)>1 and self.parse[phrase[1]].pos.lower() =="to":
                return "Sto"
            elif self.parse[phrase[0]].word.lower() == "for" and  \
                "to" in [self.parse[x].pos.lower() for x in phrase]:
                return "Sforto"
            else:
                return "Sfin"
        elif cat.startswith("Adv"):
            return "AVP"
        elif cat.startswith("A"):
            return "A"
        elif cat.startswith("V"):
            if self.parse[phrase[0]].pos.lower() == "to":
                return "VPto"
            elif self.parse[node].pos.lower() in ["vbg", "vbn"]:
                return "VPed"
            else:
                return "VPfin"
        elif cat.startswith("P"):  # return the preposition 
            return  self.parse[phrase[0]].word.lower() #self.parse[node].word
        else: 
            return "???"

    def getDeepSubcat(self, node):
        """
        Get subcategorization information for a given node number.
        """
        dependents =[]
        esubjplus = 0
        if "DSubj" in self.parse[node].attributes:
            esubjplus = int(self.parse[node].attributes["DSubj"])
        for deps in self.derivation_tree[node].values():
            dependents+=deps
        
        dependents += [node]

        depSynFunctions = [] 
        dependents.sort() # Sort to preserve word order

        foundCoAnchor = False #Hack for co-anchors
        coAnchorPre = ""
        coAnchorPos = ""

        isPassive = False
        if "voice" in self.parse[node].attributes:
            if self.parse[node].attributes["voice"] == "pas":
                isPassive = True

        # Null instantiation of Agent role in passive
        if (not "0" in self.parse[node].attributes["DSub"]): 
            depSynFunctions.append((None, "Ext"))

        for dep in dependents:
            if dep==node: 
                depSynFunctions.append((node, "Self"))
            #print dep, self.parse[dep].word
            elif self.parse[dep].tree == "tCO":
                foundCoAnchor = True
                coAnchorPrep = self.parse[dep].word
                coAnchorPos = dep
            elif self.parse[dep].attributes["DRole"]=="0":
                    depSynFunctions.append((dep, "Ext"))
            elif self.parse[dep].attributes["DRole"]=="1":
                    if esubjplus==dep: 
                        depSynFunctions.append((dep, "Ext"))
                    elif isPassive:
                        depSynFunctions.append((dep, "Obj(hyp)"))
                    else:
                        depSynFunctions.append((dep, "Obj"))
            else: 
                if foundCoAnchor: 
                    foundCoAnchor = False
                    depSynFunctions.append(((coAnchorPos, dep),"Dep(%s)"%coAnchorPre))
                else:
                    phraseType = self.assignPhraseType(dep)
                    if phraseType: 
                        depSynFunctions.append((dep, "Dep(%s)"% phraseType))
        return depSynFunctions        

    def toGraphviz(self):
        result = "digraph sentence {\n"
        
        possiblefee = []
        for node in self.derivation_tree:
            if "DSub" in self.parse[node].attributes and \
                self.parse[node].attributes["DSub"] != "nil":
                    possiblefee.append('"'+str(node)+"-"+self.parse[node].word+"-"+self.assignPhraseType(node)+'"')
        if possiblefee:
            result += 'node [color="red"]; '+' '.join(possiblefee)+";\n" 
        result += 'node [color="black"]\n';
 
        for node in self.derivation_tree:
            if node!=self.root_node: 
                if (self.parse[node].attributes["DRole"]=="Adj"):
                    style = "dashed"
                else: 
                    style = "solid"
                if "DSubj" in self.parse[node].attributes:
                    parent = int(self.parse[node].attributes["DSubj"])
                    result+=' "%s" -> "%s" [label = "%s", style = "%s"];\n' % (str(parent)+"-"+self.parse[parent].word+"-"+self.assignPhraseType(self.parse[parent]),\
                                                                          str(node)+"-"+self.parse[node].word+"-"+self.assignPhraseType(node),\
                                                                          "DSubj","dotted")

                result+=' "%s" -> "%s" [label = "%s", style = "%s"];\n' % (str(self.parse[node].parent_id)+"-"+self.parse[node].parent_word+"-"+self.assignPhraseType(self.parse[node].parent_id),\
                                                                      str(node)+"-"+self.parse[node].word+"-"+self.assignPhraseType(node),\
                                                                      self.parse[node].attributes["DRole"],style)
        result += "}"
        return result


    def showGraph(self):
        file("tmp.dot","w").write(self.toGraphviz())
        os.system("dot -Tpng -o tmp.png tmp.dot")
        os.system("qiv tmp.png")
        os.system("rm tmp.dot tmp.png")

#deps = DepsFile(file(sys.argv[1],"r"))

#sent = deps.next_sentence()
#x = DerivationTree(sent[0])

# Get the lemmatizer Singleton and load a verb list for control verbs
#lemmatizer = produceLemmatizer()
#lemmatizer.loadWordList("controlVerbs", CONTROL_VERB_LIST) 

