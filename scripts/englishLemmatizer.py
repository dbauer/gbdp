MORPH_TABLE = "/nlp/tools/english-lemmatizer/morph_english.roots2infl-corrected-noidents"

class EnglishLemmatizer():
    """
    A simple lemmatizer, that just looks up a form in the table and returns the
    lemma. If no entry is found it returns the input form (this makes sense
    because word forms that are equal to the lemma are not in the table)
    """
    
    __lemmatizerSingleton = None
    
    def __init__(self,morphTableLoc=MORPH_TABLE):

        if EnglishLemmatizer.__lemmatizerSingleton: 
            raise EnglishLemmatizer.__lemmatizerSingleton

        morphTableFile = file(morphTableLoc,"r")
        
        self.morphTable = {}

        line = morphTableFile.readline().strip()
        while line:
            lexInfo, form = line.split("#")
            form = form.strip()
            lexInfoField = lexInfo.split()
            word = lexInfoField[0].strip()
            pos = lexInfoField[1].strip()
            self.morphTable[(form, pos)] = word.strip()
            line = morphTableFile.readline().strip()

        self.wordLists = {} # Allows to store word lists 
                            # (for example stop words etc.)

        EnglishLemmatizer.__lemmatizerSingleton = self

    @staticmethod
    def getSingleton():
        return EnglishLemmatizer.__lemmatizerSingleton

    def process_word(self, form, pos):
        if (form, pos) in self.morphTable:
            return self.morphTable[(form,pos)]
        else: 
            return form

    def loadWordList(self, name, path):
        """
        Load a word list and store it under the provided Id.
        Word lists contain one lemmatized word per line.
        """
        words = set([w.strip() for w in file(path).readlines()])
        self.wordLists[name] = words

    def loadControlWordList(self, nameSubj, nameObj, path):
        words = [w.strip().split() for w in file(path).readlines()]
        subjectset = set()
        objectset = set()
        for t,w in words: 
            if "S" in t or "A" in t:
                subjectset.add(w.strip())
            else: 
                objectset.add(w.strip())
        self.wordLists[nameSubj] = subjectset
        self.wordLists[nameObj] = objectset

    def isInWordList(self, list, lemma):
        return lemma in self.wordLists[list] 

    def hasWordList(self, name):
        return name in self.wordLists
         


