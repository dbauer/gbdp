#!/usr/bin/python
from collections import defaultdict
#from morphaWrapper import MorphaWrapper;
import re
import sys
import copy

TREEBANK_TYPES_TO_CRISP = { 's': 'substitution', 
                            'f': 'foot',
                            'h': 'anchor',
                            'c': 'lex'}

NO_SEM_POS_LIST =[] #['IN','DT','USCORE']

def normalize_name(name):
   
    name = re.sub(",","_COMMA_",name)
    name = re.sub("\.","_DOT_",name)
    name = re.sub("\?","_QMARK_",name)
    name = re.sub("!","_EXCLMARK_",name)
    name = re.sub('[\'`"]',"_QUOT_",name)
    name = re.sub(':',"_COLON-",name)
    name = re.sub('/',"_SLASH_",name)
    name = re.sub('\\\\',"_BSLASH_",name)
    name = re.sub("&","_AMP_",name)
    name = re.sub("=","_EQUALS_",name)
    name = re.sub("-","_",name)
    name = re.sub("[^a-zA-Z0-9\_]","_UNKN_",name)

    if re.match("[^a-zA-Z]",name):
        name = "d"+name
    
    name = name.lower()
    #if name2[0] in "1234567890_":
    #    name2 = "d"+name2
    return name    
                            
class pTAGNode(list):
    """ 
    Represents an individual node in a TAG tree.
    """

    def __init__(self, aCat, aRole, aNum, aType, aParent):
        self.cat, self.role, self.num, self.ntype = aCat, aRole, aNum, aType
        # remember parent nodes for easy bottom up traversal
        if self.ntype == "f":
            self.role = None
        self.parent = aParent
        self.constraint = ""        
        self.cat=normalize_name(self.cat)       
            

    def is_leaf(self):
        return len(self)==0
        
    def __str__(self):
        return self.cat+"#"+self.role+"#"+self.num+"#"+self.ntype
    
    def getDesc(self):
        return self.__str__()

    def getDtrs(self):
        return self

    def CRISPstr(self):
        """
        Create XML code to represent this subtree.
        """
        return self.rec_CRISPstr(0)
        
    def rec_CRISPstr(self, indent):
        
        result = indent*' '
        
        if self.is_leaf():  
            result += '<leaf cat="' + self.cat + '" id="' + self.num + '"' 
            if self.ntype:
                result += ' type="'+ TREEBANK_TYPES_TO_CRISP[self.ntype] + '"'             
            if self.role:
                result += ' sem="' + self.role + '"'
            if self.constraint:
                result += ' constraint="' + self.constraint + '"'
            result += '/>'
            
        else: 
            result += '<node cat="' + self.cat + '" id="' + self.num + '"' 
            if self.role:
                result += ' sem="' + self.role + '"'
            result += '>\n'
                        
            for child in self:
                result += child.rec_CRISPstr(indent+2) + '\n'
            
            result += ' '*indent + '</node>'
        return result
            
        
    def compute_roles(self):
        """
        Assign a role to this node
        """    
        
        # It is possible that some nodes that should be assigned "self" get
        # no role and instead a null adjunction constraint. This is the 
        # more conservative option as it avoids ungrammatical construnctions
        # and hopefully not too many important constructions will be lost.
       
        if self.is_leaf(): # this is a leaf node
            if not self.role: # If the role is marked already, stop here 
                if self.ntype == "c":
                        self.constraint = "na" # Otherwise this is a co anchor 
                if self.ntype in ["h","f","c"]: # TODO
                    self.role = "self" # Heads and foot nodes always get 'self' as a role
                    return [(self.num, "self")]
                elif not self.ntype or  self.ntype in ["s"]: #This should never happen
                        #print "WARNING: substitution node without role, assigned 'self'"
                        self.role = "self" 
                        return [(self.num, "self")]
                #        return [(self.num, "self")]                   
                return []
            else:
                return [(self.num, self.role)] 
        else: # no leaf node
            roles = []
            for child in self:
                roles += child.compute_roles()

            child_roles = [child.role for child in self]            
            
            child_roles.sort(lambda x,y: (x<=y) - 1)       # ??? 
            self.role = child_roles[0]
            roles.append((self.num, self.role))
            return roles 
   
                                              

class pTAGTree:
    """
    Represents a single TAG tree.
    """
    
    def __init__(self, tree_string):
        """
        Create a new pTAGTree from a tree description (a line from 
        the .str file)
        """
        node_strings = tree_string.strip().split(" ") 
        self.id = node_strings[0] # First element contains tree id
        self.num_subst_nodes = 0
        self.id_to_node = {}
        self.root_node = self.create_tree(node_strings[1:])        
        self.roles = [] 
        self.node_to_roles = {}       
               
        self.subst_counts = {}
        self.adj_counts = {}
        self.init_count = {}

        self.anchor_pos = ""

    def is_left_adjoining(self):
        return self.get_leaf_nodes()[0].ntype=="f"         

    def is_right_adjoining(self):
        return self.get_leaf_nodes()[-1].ntype=="f"

    def is_nonwrapping(self):
        return self.is_left_adjoining() or self.is_right_adjoining()
        

    def create_tree(self,node_strings):
        done = []
        open_node = []

        self.tree_type = "init" # assume this is an elementary tree until further evidence
        
        for node_string in node_strings:
            
            if node_string[0]=="#": # in rare cases the category is marked as '#'               
                cat = "NONE"
                role, num, pos, ntype = node_string[2:].split("#")
            else:     
                cat, role, num, pos, ntype = node_string.split("#")
                cat = normalize_name(cat)                                
            num = "n"+num #normalize node numbers
            if role:
                role = "r"+role #normalize role names
            
            if ntype == "f": # if a node is marked as footnode the tree has to be an auxiliary tree
                self.tree_type = "aux"
            if pos=="l":     
                new_node = pTAGNode(cat, role, num, ntype, open_node)
                if ntype in "hfcs":
                    new_node.constraint = "na"
                if ntype=="h":
                    self.anchor_pos = cat
                self.id_to_node[num] = new_node # Nodes can be accessed by their number                
                open_node.append(new_node)
                done.append(open_node)
                open_node = new_node
            if pos=="r":
                open_node = done.pop()

            if ntype == "s":
                self.num_subst_nodes += 1
            
        return open_node[0] 
                                                      
    def getDesc(self):
        return self.id
        
    def get_subst_nodes(self):
        return self.rec_get_subst_nodes(self.root_node)

    def rec_get_subst_nodes(self, node):
        nodes = []        
        if node.ntype == "s":
           nodes += [node]
        else:
            for child in node:
                nodes += self.rec_get_subst_nodes(child)
        return nodes
    
    def get_non_subst_nodes(self):
        return self.rec_get_non_subst_nodes(self.root_node)

    def rec_get_non_subst_nodes(self, node):
        nodes = []        
        if node.ntype != "s":
           nodes += [node]
        
        for child in node:
            nodes += self.rec_get_non_subst_nodes(child)
        return nodes        
        
    def get_leaf_nodes(self):
        return self.rec_get_leaf_nodes(self.root_node)
    
    def rec_get_leaf_nodes(self, node):       
        if len(node)==0:            
            return [node]
        leaf_nodes = []    
        for child in node:            
            leaf_nodes += self.rec_get_leaf_nodes(child)                   
        return leaf_nodes
        
    def compute_roles(self):
        roles =  self.root_node.compute_roles()
        #print self.id, roles
        self.roles = set(roles)
        self.node_to_roles = dict(roles)
        
    def CRISPstr(self):
        """
        Create XML code to represent this tree
        """
        
        result = '<tree id="'+self.id+"x"+self.anchor_pos+'">\n'
        result += self.root_node.rec_CRISPstr(2)+ '\n'
        if self.init_count: 
            result += '  <prob type="init">'+str(self.init_count)+'</prob>\n'
    
        for node, tree in self.subst_counts:            
            role = self.id_to_node[node].role
            cat = self.id_to_node[node].cat
            result += '  <prob type="subst" tree="'+tree+'" node="'+node+'" sem="'+role+'" cat="'+cat+'">' +\
                str(self.subst_counts[(node,tree)]) + '</prob>\n'
        
        for node,tree in self.adj_counts:
            role = self.id_to_node[node].role
            cat = self.id_to_node[node].cat
            if tree == None:
                result += '  <prob type="noadjoin" node="'+node+'" sem="'+role+'" cat="'+cat+'">' +\
                  str(self.adj_counts[(node,tree)]) + '</prob>\n'                    
            else:                                                   
                result += '  <prob type="adjoin" tree="'+tree+'" node="'+node+'" sem="'+role+'" cat="'+cat+'">'                        
                result += str(self.adj_counts[(node,tree)]) + '</prob>\n'
        result += '</tree>'
        
        return result
                                   
    
        
        
class pTAGLexEntry():
    
        
    def compute_sem_content(self,tree):
       
        if not tree in self.semcontent:
            semcontent = ""
            if not self.pos in NO_SEM_POS_LIST:
                roleset = set([x[1] for x in tree.roles if x[1]])

                semcontent += self.word+"_"+str(len(roleset))
                semcontent += "(self"
                roles = [x[1] for x in tree.roles]
                roles.sort()
                for role in roles:
                    if role and role!="self":
                        semcontent += "," + role
                semcontent += ")"
            self.semcontent[tree] = semcontent
        return self.semcontent[tree]    
        
        
    def __init__(self, aWord, aPos):
        self.word = normalize_name(aWord)
        self.pos = normalize_name(aPos)        
        
        self.trees = []
        self.semcontent = {}        

        # store how often each tree occurs as parent and child tree of substitution and adjunction in total
        self.subst_parent_counts = defaultdict(lambda: defaultdict(int))
        self.subst_child_counts = defaultdict(int)
        self.adj_parent_counts = defaultdict(lambda: defaultdict(int))
        self.adj_child_counts = defaultdict(int)

        # semantic requirements for each lexicalized tree
        self.semreqs = defaultdict(list)        
        # probabilities for this entry
        self.occurenceCount = defaultdict(int)
        self.init_count = defaultdict(int)   
        # Default dictionaries to store subst  and adjunction probabilities for
        # all lexicalized tree with this lexicon entry
        self.subst_counts = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))                                             
        self.adj_counts = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))                                                  
        self.noAdjCount = defaultdict(lambda: defaultdict(int))        
     
    def add_tree(self,tree):
        self.trees.append(tree)
        
    def CRISPstr(self):
        """
        Create XML code to represent this entry.
        """
        result = '<entry word="'+self.word+'" pos ="'+self.pos+'">\n'
        

        for tree in self.trees:
            treename = tree.id + "x"+tree.anchor_pos
            result += '  <tree refid="'+treename+'">\n'
            semcontent = self.compute_sem_content(tree)
            if semcontent:
                result += '    <semcontent>'+semcontent+'</semcontent>\n'
            for req in self.semreqs:
                result += '    <semreq>'+req+'</semreq>\n'
    
             
            result += '    <prob type="childsubstprior">'+str(self.subst_child_counts[treename])+'</prob>\n'
            result += '    <prob type="childadjprior">'+str(self.adj_child_counts[treename])+'</prob>\n'
            for node in self.subst_parent_counts[treename]:
                result += '    <prob type="parentsubstprior" node="'+node+'">'+str(self.subst_parent_counts[treename][node])+'</prob>\n'
            for node in self.adj_parent_counts[treename]:
                result += '    <prob type="parentadjprior" node="'+node+'">'+str(self.adj_parent_counts[treename][node])+'</prob>\n'

            if self.init_count[treename]: 
                result += '    <prob type="init">'+str(self.init_count[treename])+'</prob>\n'
            
            for node in self.subst_counts[treename]:            
                role = tree.id_to_node[node].role
                cat = tree.id_to_node[node].cat
                for t, word in self.subst_counts[treename][node]:
                    result += '    <prob type="subst" lex="'+word+'" tree="'+t+'" node="'+node+'" sem="'+role+'" cat="'+cat+'">' +\
                        str(self.subst_counts[treename][node][(t,word)]) + '</prob>\n'
            
            for node in self.adj_counts[treename]:
                role = tree.id_to_node[node].role
                cat = tree.id_to_node[node].cat
                for t, word in self.adj_counts[treename][node]:
                    if t == None and word==None:
                        result += '    <prob type="noadjoin" node="'+node+'" sem="'+role+'" cat="'+cat+'">' +\
                            str(self.adj_counts[treename][node][(t,word)]) + '</prob>\n'                    
                    else:                                                   
                        result += '    <prob type="adjoin" lex="'+word+'" tree="'+t+'" node="'+node+'" sem="'+role+'" cat="'+cat+'">'                        
                        result += str(self.adj_counts[treename][node][(t,word)]) + '</prob>\n'
        
            result += '  </tree>\n'
        result += '</entry>'
        
        return result
    
class TAGrammar(dict):
    """
    Represent a Tree Adjoining Grammar.
    """

    def __init__(self,infile):
        
        self.lexicon = defaultdict() # Dictionary from (treeID, word) --> lex entries
        
        lines = infile.readlines()
       
        self.anchor_pos_dict = {}

        self.category_counts = defaultdict(int) 
        self.category_noadj_counts = defaultdict(int)

        # Dummy tree
        self["tCO"] = pTAGTree("tCO NP##1#l# NP##1#r#")
 

        for line in lines:
            string = line.strip()            
            if string: #ignores empty lines
                string = re.subn  ("[\.\&]","_",string)[0];
                #try:
                new_tree = pTAGTree(string) 
                new_tree.compute_roles() #annotate roles in the new tree
                self[new_tree.id] = new_tree
                self.anchor_pos_dict[new_tree.id] = set()
                #except:
                #   print "Warning: Could not parse"
                #  print "   "+string
       
            
                    
    def CRISPstr(self):
        """
        Return an iterator over XML stings describing trees in the grammar.               
        """        
        yield "<crisp-grammar>\n"
       
        for cat in self.category_counts: 
            yield '<prob type="noadjgivencat" cat="'+cat+'" >'+str(self.category_noadj_counts[cat])+'</prob>'
 
        yield "\n"
        for tree_id in self:
                tree = self[tree_id]
                yield tree.CRISPstr()+"\n"
        
        for lex_entry in self.lexicon:
            yield self.lexicon[lex_entry].CRISPstr()+"\n"
        yield "</crisp-grammar>"
        
