#ifndef LOG_H
#define LOG_H

struct Double{        
    Double(){};
    Double(double) { }; //implicit conversion from double 
    operator double(); //implicit conversion to double 
}; 

// Addition in log space
double logadd(double a, double b);

#endif
