#include "logprob.h"
#include <cmath>
#include <iostream>

double logadd(double a, double b) {
    if (a > b) { // make sure b is the greater item
        double c = b;
        b = a;
        a = c;    
    } 
     
    return a + log1p(std::exp(b-a));
}
