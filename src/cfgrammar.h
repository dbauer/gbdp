#ifndef CFGPARSER_DRIVER_H
#define CFGPARSER_DRIVER_H
#include <vector>
#include "rule.h"
#include <string>
#include <unordered_set>
#include <unordered_map>

using namespace std;

namespace cfgparser{
class CfGrammar {

    public:
        CfGrammar(); 
        void add_rule(Rule* rule_ptr);
        static CfGrammar* load(string filename);
        string filename;
        vector<Rule> rules;

        unordered_map<string, unordered_set<int> > terminal_to_rule_ids; // todo: could use unordered_multimap here
        unordered_set<int> nonterminal_rules; 
        unordered_map<string, int> epsilon_rules; 
    private: 
        int rule_counter;
        void initialize_terminal_lookup();

 
};
}
#endif /*CFGPARSER_DRIVER_H*/
