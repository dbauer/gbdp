#include <iostream>
#include "earley.h"
#include "cfgrammar.h"
#include <sstream>
#include "logprob.h"
#include <iterator>

/**
 * Split a string. Source: http://stackoverflow.com/questions/2275135/splitting-a-string-by-whitespace-in-c
 */
std::vector<std::string> split(std::string const &input) { 
    std::istringstream buffer(input);
    std::vector<std::string> ret((std::istream_iterator<std::string>(buffer)), 
                                 std::istream_iterator<std::string>());
    return ret;
}

int main(int argc, char *argv[]) {



    cfgparser::CfGrammar* grammar = cfgparser::CfGrammar::load(string(argv[1]));
    cout << "loaded grammar." << endl;
    earley::EarleyParser earleyParser(*grammar);
    
    //for (std::vector<cfgparser::Rule>::iterator rule_itr = grammar->rules.begin(); rule_itr!=grammar->rules.end(); rule_itr++){
    //    Rule rule = *rule_itr;
    //    cout << rule << " "<< rule.weight << endl ;
    //}

    //earleyParser.parse_input(split("the tall green man saw the hill with the tall powerful green telescope")); 
   // earleyParser.parse_input(split("t29 t27 t1 t3 t4 t1 t3 t26"));
    //earleyParser.parse_input(split("t90 t20 t5 t105 t105 t791 t2 t3 t792 t23 t50 t13 t106 t442 tCO t1 t3 t669 t85 t2 t3 t21 t2 t2 t38 t4 t2 t2 t2 t3 t21 t100 t99 t439 t519 t35 t35 t9 t383 t35 t35 t19 t4 t2 t2 t36 t3"));
    //earleyParser.parse_input(split("t26"));
    //earleyParser.parse_input(split("t2 t3 t21 t2 t186 t691 t295 t36 t229 t4 t2 t2 t2 t3 t21 t23 t129 t1 t36 t188 t4 t1 t36 t36 t3 t26"));
    cout << endl;

    print_forest(earleyParser.get_packed_forest());
    delete grammar;
}
