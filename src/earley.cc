#include "earley.h"
#include "item.h"
#include "logprob.h"
#include <iostream>
#include <deque>
#include <queue>
#include <algorithm>

typedef cfgparser::Rule Rule;
typedef cfgparser::CfGrammar CfGrammar;

//#define DEBUG

namespace earley {

    double BEAM_RATIO = 0.001;


	EarleyParser::EarleyParser(const CfGrammar& gr) : grammar(gr) {
        startsymbol = gr.rules.at(0).lhs;
    };

   
    /**
     * This is the main parsing method. 
     */
	int EarleyParser::parse_input(const vector<string>& input) {
        initialize_nt_table(input);

        
		initialize_chart(input);
        bool success = false;

        for (chart_type::size_type i(0); i <= input.size(); i++) { // for each row on the chart
            #ifdef DEBUG
            cout << "----"<< i << "----"<<endl;
            #endif 
                   
            std::vector<Item> all_items(chart.at(i).begin(), chart.at(i).end()); 

            
            CompareScores comparator(inside_lp);
            std::sort(all_items.begin(), all_items.end(), comparator);

            double max = compute_figure_of_merit(all_items.at(0));
            double min = compute_figure_of_merit(all_items.at(all_items.size()-1));
            double ratio = (max/min);
            //cout << "MAX "<< max<<endl;
            //cout << "RATIO "<< ratio << endl; 
 
            std::unordered_set<Item,hash_item> items;
            for(std::vector<Item>::iterator it = all_items.begin(); it != all_items.end(); it++) { 
                Item item = *it;
                double fom = compute_figure_of_merit(item);
                if (fom > max / BEAM_RATIO) {
                    items.insert(item);
                } 
            }
            
            std::deque<Item> q; 
            for(std::unordered_set<Item,hash_item>::iterator it = items.begin(); it!= items.end(); it++) // copy items to the queue 
                q.push_back(*it);

            while (!q.empty()) {
                Item item = q.front();
                q.pop_front();
                if (item.is_closed()) {
                   #ifdef DEBUG
                   cout << "CLOSED ITEM: " << item<< " "<< inside_lp[item] <<endl;
                   #endif 
                   unordered_set<Item,hash_item>& potential_completes = chart.at(item.left);
                   // TODO: Speed this up by keeping a table of outside nonterminals -> items
                   for (unordered_set<Item,hash_item>::iterator incomplete_item = potential_completes.begin(); incomplete_item!=potential_completes.end(); incomplete_item++)  {
                        Item complete_this = *incomplete_item;
                        if (!complete_this.is_closed() && complete_this.get_outside().nonterminal.compare(item.rule.lhs) == 0) {
                            //Item* test_item_ptr = new Item(complete_this.complete(item)); 
                            Item test_item = Item(complete_this.complete(item));// *test_item_ptr;
                            #ifdef DEBUG
                            cout << "COMPLETE " << complete_this << " "<< inside_lp[complete_this]<< endl; 
                            #endif
                            unordered_set<Item,hash_item>::iterator found_item_ptr = items.find(test_item); // check if this item exists

                            dot_bps[test_item].push_back(complete_this);
                            complete_bps[test_item].push_back(item);
                            if (found_item_ptr == items.end()) { // item not found
                                items.insert(test_item);
                                chart.at(i).insert(test_item);
                                q.push_back(test_item);  
                                inside_lp[test_item] = inside_lp[complete_this] + inside_lp[item]; // Update inside probability
                            } else 
                                inside_lp[test_item] = logadd(inside_lp[test_item], inside_lp[complete_this] + inside_lp[item]); // Update inside probability
                            #ifdef DEBUG
                            cout << "new item: " << test_item << " "<< inside_lp[test_item]<< endl;
                            #endif

                            if (complete_this.can_skip()) { // after complete, create item without moved dot, if permissible 
                                Item no_skip_item = Item(complete_this.rule, complete_this.left, item.right, complete_this.dot); 
                                unordered_set<Item,hash_item>::iterator found_item_ptr = items.find(no_skip_item); // check if this item exists
                                
                                dot_bps[no_skip_item].push_back(complete_this);
                                complete_bps[no_skip_item].push_back(item);
                                inside_lp[no_skip_item] = inside_lp[test_item];
                                if (found_item_ptr == items.end()) { // item not found
                                    items.insert(no_skip_item);
                                    chart.at(i).insert(no_skip_item);
                                    q.push_back(no_skip_item);
                                }
                            #ifdef DEBUG
                            cout << "new item (no skip): " << no_skip_item << " "<< inside_lp[no_skip_item]<< endl;
                            #endif
                            }
                        }            
                    }
                } else { 
                    #ifdef DEBUG
                    cout << "OPEN ITEM: " << item << inside_lp[item] <<endl;
                    #endif

                    if (item.can_skip()) {
                        Item test_item = Item(item.skip());
                        unordered_set<Item, hash_item>::iterator found_item_ptr = items.find(test_item);
                        complete_bps[test_item] = complete_bps[item]; // copy the back pointers
                          
                        float epsilon_weight; 
                        string outside_nt(item.get_outside().nonterminal);
                        unordered_map<string, int>::iterator found_epsilon_ptr = grammar.epsilon_rules.find(outside_nt);
                        if (found_epsilon_ptr!=grammar.epsilon_rules.end())
                            epsilon_weight = grammar.rules.at((*found_epsilon_ptr).second).weight;
                        else
                            epsilon_weight = 0.0; // If no special rule exists for null adjunction, assume log P = 0.0 
                     
                        dot_bps[test_item] = dot_bps[item]; 
                        #ifdef DEBUG
                        cout << "SKIP" << endl;
                        #endif
                        if (found_item_ptr == items.end()) { 
                            items.insert(test_item);
                            chart.at(i).insert(test_item);
                            q.push_back(test_item);
                        }
                        inside_lp[test_item] = inside_lp[item] + epsilon_weight;
                        #ifdef DEBUG
                        cout << "new item: " << test_item << " "<< inside_lp[test_item]<< endl;
                        #endif
                    }

                    // Check if a nonterminal can be skipped
                    if (i< input.size()) {

                        if (item.can_shift(input.at(i))) { // SHIFT
                            #ifdef DEBUG
                            cout <<  "SHIFT" << endl;
                            #endif
                            Item test_item = Item(item.shift());
                            unordered_set<Item,hash_item>::iterator found_item_ptr = items.find(test_item); // check if this item exists
                            complete_bps[test_item] = complete_bps[item]; // copy the back pointers
                            dot_bps[test_item] = dot_bps[item]; 
                            if (found_item_ptr== items.end()) { 
                                chart.at(i+1).insert(test_item);
                                items.insert(test_item);
                                inside_lp[test_item] = inside_lp[item];
                            }
                            #ifdef DEBUG
                            cout << "new item: " << test_item << " "<< inside_lp[test_item]<< endl;
                            #endif
                              
                        } else if (item.outside_is_nt()) {  // PREDICT
                            #ifdef DEBUG
                            cout <<  "PREDICT " << endl;
                            #endif
                            unordered_set<int> new_rules = lhs_to_rule_ids[item.get_outside().nonterminal];
                            for (unordered_set<int>::iterator rid = new_rules.begin(); rid != new_rules.end(); rid++) {
                                Item new_item = Item(grammar.rules.at(*rid),i); 
                                #ifdef DEBUG
                                cout << "new item: " << new_item << " "<< inside_lp[new_item]<< endl;
                                #endif
                                if (items.count(new_item) == 0) {
                                    q.push_back(new_item);
                                    items.insert(new_item);
                                    chart.at(i).insert(new_item);
                                    inside_lp[new_item]= new_item.rule.weight;
                                }                         
                            }
                            
                        }
                      }
              }   
            }     
        }
        return 1;

	};
	
    void EarleyParser::initialize_nt_table(const vector<string>& input) {
        lhs_to_rule_ids.clear(); 
        for(vector<string>::const_iterator t = input.begin(); t != input.end(); t++) {
            // TODO: deal with unseen terminals. The preprocessor should make sure these 
            //   do not exist, but there should be a failsafe test.
            string token = *t;
            unordered_set<int>& rules_for_token = grammar.terminal_to_rule_ids[token];
            for(unordered_set<int>::iterator rid = rules_for_token.begin(); rid != rules_for_token.end(); rid++) {
                Rule& rule = grammar.rules.at(*rid);
                unordered_set<int>& lhs_rule_set = lhs_to_rule_ids[rule.lhs];
                lhs_rule_set.insert(rule.id);
            }
        }
        
        for(unordered_set<int>::const_iterator rid = grammar.nonterminal_rules.begin(); rid != grammar.nonterminal_rules.end(); rid++) {
            Rule& rule = grammar.rules.at(*rid);
            unordered_set<int>& lhs_rule_set = lhs_to_rule_ids[rule.lhs];
            lhs_rule_set.insert(rule.id);
            lhs_to_rule_ids[rule.lhs] = lhs_rule_set;
        }

    }

	void EarleyParser::initialize_chart(const vector<string>& input) {
        chart.clear();
       
        unordered_set<Item,hash_item> items;
        unordered_set<int>& rules = lhs_to_rule_ids[startsymbol];

		for(unordered_set<int>::iterator rid = rules.begin(); rid != rules.end(); rid++){
            //Item* new_item_ptr = new Item(grammar.rules.at(*rid),0);
            //items.insert(*new_item_ptr);
            Item new_item(grammar.rules.at(*rid),0);
            items.insert(new_item);
            inside_lp[new_item] = new_item.rule.weight;
            
		}	
		chart.push_back(items);
        // add empty rows to chart;
        for (chart_type::size_type i(0); i < input.size(); i++)
            chart.push_back(unordered_set<Item,hash_item>());
 
	}
    
    /*typedef std::vector< std::unordered_set<Item,hash_item> > chart_type;
    typedef std::vector< std::vector<earley::Item> > forest_entry_type;
    typedef unordered_map<earley::Item, forest_entry_type, hash_item> forest_type; */

    forest_type EarleyParser::get_packed_forest() {
        /*
        // print out back pointer tables
        for (unordered_map<Item, vector<Item>, hash_item>::iterator kv_ptr = complete_bps.begin(); kv_ptr != complete_bps.end(); kv_ptr++) {
            cout << kv_ptr->first << ": ";
            vector<Item> value = kv_ptr->second;
            for (vector<Item>::iterator item_ptr = value.begin(); item_ptr!=value.end(); item_ptr++) {
                cout << *item_ptr << " ";
            }
            cout << endl;
        }
        cout << endl;
        cout << endl;
        
        for (unordered_map<Item, vector<Item>, hash_item>::iterator kv_ptr = dot_bps.begin(); kv_ptr != dot_bps.end(); kv_ptr++) {
            cout << kv_ptr->first << ": ";
            vector<Item> value = kv_ptr->second;
            for (vector<Item>::iterator item_ptr = value.begin(); item_ptr!=value.end(); item_ptr++) {
                cout << *item_ptr << " ";
            }
            cout << endl;
        }
        cout << endl;
        */

        // get the items covering the full span that start with the startsymbol
        std::unordered_set<Item, hash_item> last_row(chart.at(chart.size()-1));
        for (unordered_set<Item,hash_item>::iterator item_ptr = last_row.begin(); item_ptr!=last_row.end(); item_ptr++)  {
            Item item = *item_ptr;
            if (item.is_closed() && item.rule.lhs.compare(startsymbol)==0){
                forest_do_completed_item(item);
            }
        }     
        return forest;
    }

    void EarleyParser::forest_do_completed_item(Item item) {
        //cout << " calling do complete for" << item << endl;
        forest_type::iterator found_ptr = forest.find(item);
        if (found_ptr == forest.end()) {  // item is not yet in the forest
            forest[item] = forest_search_completions(item);
        }
    }

    vector<vector<Item>> EarleyParser::forest_search_completions(Item item) {
        forest_entry_type result;  

        //cout << "calling search completions for " << item << endl;
        vector<Item> dot_bp = dot_bps[item];
        vector<Item> complete_bp = complete_bps[item];
        for (int i=0; i<dot_bp.size(); i++) {            
            Item dot_item = dot_bp.at(i);
            //cout << "dot bp is" << dot_item << endl;
            Item last_compl = complete_bp.at(i);
            forest_do_completed_item(last_compl);

            if (complete_bps[dot_item].size() > 0) {
                vector<vector<Item>> splits = forest_search_completions(dot_item);
                for(vector<vector<Item>>::iterator split_ptr = splits.begin(); split_ptr!=splits.end(); split_ptr++) {
                    vector<Item> split(*split_ptr); // copy contents of the split
                    split.push_back(last_compl);
                    result.push_back(split);    
                }
            } else {
                vector<Item> split;
                split.push_back(last_compl);
                result.push_back(split);
            }
        }
        return result;
    }

    double EarleyParser::compute_figure_of_merit(const Item& item) {
       return inside_lp[item] / (item.right-item.left+1); 
    } 


    void nbest(forest_type forest) {
        
    }

    void print_forest(forest_type forest) {
        for (forest_type::iterator kv_ptr = forest.begin(); kv_ptr != forest.end(); kv_ptr++) {
            pair<Item, forest_entry_type> kv(*kv_ptr);
            forest_entry_type children(kv.second);
            if (children.size()>0) {
                for (forest_entry_type::iterator split_ptr = children.begin(); split_ptr != children.end(); split_ptr++) {
                    cout << kv.first << " -> ";
                    for (vector<Item>::iterator child_ptr = split_ptr -> begin(); child_ptr != split_ptr -> end(); child_ptr++) {
                        cout << *child_ptr << " ";
                    }
                    cout << endl;
                }
            } else {
                cout << kv.first << endl;
            }   
        }
    }

	
}

