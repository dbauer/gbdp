#include "item.h"

typedef cfgparser::Rule Rule;

namespace earley {

    Item::Item(const Rule& theRule, int l, int r, int d) : rule(theRule), left(l), right(r), dot(d){ }

    Item::Item(const Rule& theRule, int pos) : rule(theRule), left(pos), right(pos), dot(0){ }; 


    bool Item::is_closed(){
        return dot >= rule.rhs.size();
    }

    bool Item::outside_is_nt() {
        return (!is_closed()) && !rule.rhs.at(dot).is_terminal;
    }

    bool Item::can_skip() {
        return outside_is_nt() && rule.rhs.at(dot).has_kleene;
    }
    

    bool Item::can_shift(string token) {
        return (!is_closed()) && 
            rule.rhs.at(dot).is_terminal &&
            token.compare(rule.rhs.at(dot).terminal) == 0;
    }
    
    Item Item::shift() {
        Item result = Item(rule, left, right+1, dot+1);      
        return result;
    }
    
    Item Item::complete(const Item& other) {
        return Item(rule, left, other.right, dot+1);   
    }

    Item Item::skip() {
        return Item(rule, left, right, dot+1); 
    }

    // Methods of RuleToken
    RuleToken& Item::get_outside() {
        return rule.rhs.at(dot);
    }


}


