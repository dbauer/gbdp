#ifndef ITEM_H
#define ITEM_H

#include "rule.h"
#include <vector> 
#include <iostream>
#include <unordered_map>
typedef cfgparser::Rule Rule;
typedef cfgparser::RuleToken RuleToken;

namespace earley {

/**
 * Earley parse item. 
 */
class Item {

  public:
    Item(const Rule& theRule, int l, int r, int d); 
    Item(const Rule& theRule, int pos);
    Item shift();
    Item complete(const Item& other);
    Item skip();
    bool is_closed();
    bool outside_is_nt(); 
    bool can_shift(string token);
    bool can_skip();
    RuleToken& get_outside();
    Rule rule;
    int left;
    int right;
    std::vector<string>::size_type dot;

    std::vector<Item> shift_bp;
    std::vector<Item> complete_bp; // points to items used to complete this one
    std::vector<Item> dot_bp; // points to previous items before the completion

    friend std::ostream& operator<<(std::ostream &strm, const Item &item) {
        return strm << "<" << item.rule <<  ", "<< item.dot << " ["<<item.left <<","<<item.right<<"]>";
    }    
  
    friend bool operator ==(Item const& i1, Item const& i2) {
        return (i1.left == i2.left) &&
           (i1.right == i2.right) && 
           (i1.rule.id == i2.rule.id) && 
           (i1.dot == i2.dot);
    }


};

struct hash_item : public std::unary_function<Item, size_t> {
    size_t operator() (const Item& item) const {return 2957 * item.rule.id + 3889 * item.dot + 4297 * item.left + 4657 * item.right; }
};


class CompareScores {

    private:
        unordered_map<Item, double, hash_item> inside_lp;

    public:        
        CompareScores(unordered_map<Item, double, hash_item> const& map) {
            inside_lp = map;
        };
        bool operator()(Item const& item1, Item const& item2) {
        // Compute figure of merit for each item and compare them.
        double item1fom = inside_lp[item1] / (item1.right - item1.left + 1);
        double item2fom = inside_lp[item2] / (item2.right - item2.left + 1);
        return item1fom > item2fom; 
    }
};


}

#endif /*ITEM_H*/
