#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <list>
#include <cmath>
#include "logprob.h"
#include "rule.h"
#include "item.h"
#include "cfgrammar.h"

typedef cfgparser::Rule Rule;

namespace earley {

    typedef vector< unordered_set<Item,hash_item> > chart_type;

    typedef vector< vector<Item> > forest_entry_type;
    typedef unordered_map<Item, forest_entry_type, hash_item> forest_type; 

	/**
	 * This is the actual parser context that implements all the parser functionality 
     * and keeps track of the parse chart etc. One is generated for each input. 
	 */
	class EarleyParser{
	
	public:
		EarleyParser(const cfgparser::CfGrammar& grammar);
		int parse_input(const vector<string>& input);         
        forest_type get_packed_forest();

	private:

        unordered_map<Item, vector<Item>, hash_item> dot_bps;
        unordered_map<Item, vector<Item>, hash_item> complete_bps;

        unordered_map<Item, double, hash_item> inside_lp; 
       
        double compute_figure_of_merit(const Item& item); 

		chart_type chart; 
        forest_type forest; 
		cfgparser::CfGrammar grammar;
        string startsymbol;
        void initialize_nt_table(const vector<string>& input);
		void initialize_chart(const vector<string>& input);

       
        unordered_map<string, unordered_set<int> > lhs_to_rule_ids; // todo: could use unordered_multimap here
    
        vector<vector<Item>> forest_search_completions(Item item);
        void forest_do_completed_item(Item item); 

	};

    void print_forest(forest_type forest);


}
