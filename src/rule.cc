#include "rule.h"

namespace cfgparser {

Rule::Rule(int the_id, const string& the_lhs, const vector<RuleToken>& the_rhs, double the_weight) : id(the_id), lhs(the_lhs), rhs(the_rhs), weight(the_weight) {}

}
