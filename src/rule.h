#ifndef RULE_H
#define RULE_H

#include<string>
#include<vector>
#include<iostream>

using namespace std;

namespace cfgparser {

struct RuleToken {
    string nonterminal;
    string terminal;
    int index;    
    bool is_terminal;
    bool has_kleene;
};

class Rule {
  public:
    Rule(int the_id, const string& the_lhs, const vector<RuleToken>& the_rhs, double the_weight); 
    int id;
    string lhs;
    vector<RuleToken> rhs;
    double weight;

    friend std::ostream& operator<<(std::ostream &strm, const Rule &rule) {
               
        strm << rule.lhs << " ->";

        for (vector<RuleToken>::const_iterator it = rule.rhs.begin(); it < rule.rhs.end(); it++) {
            RuleToken token = *it;
            strm << " ";
            if (token.is_terminal)
                strm <<  token.terminal;

            else 
                if (token.has_kleene)
                    strm << "NT("<< token.nonterminal<<")*";
                else
                    strm << "NT("<< token.nonterminal<<")";
        }
        //strm << " " << rule.weight;
        return strm;
    };    
  
};  

    
}
#endif /* RULE_H */
