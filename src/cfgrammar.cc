#include "cfgrammar.h"
#include "cfgparser.h"
#include <string>
#include <iostream>

namespace cfgparser {
    CfGrammar::CfGrammar() : rule_counter(0) {}
    
    void CfGrammar::add_rule(Rule* rule_ptr) {  
        Rule rule = *rule_ptr; 
        rule.id = rule_counter++;
        rules.push_back(rule); 
    }

    CfGrammar* CfGrammar::load(std::string filen) {
        CfGrammar* grammar = new CfGrammar();
        grammar->filename = filen;
        yy::parser parser(grammar); 
        parser.parse();
        grammar->initialize_terminal_lookup();
        return grammar;
    }

    void CfGrammar::initialize_terminal_lookup() {
        terminal_to_rule_ids.clear();
        nonterminal_rules.clear();

		for(vector<Rule>::iterator r = rules.begin(); r != rules.end(); r++) {
            Rule rule = *r;
            bool found_terminal(false);

            if (rule.rhs.size() == 0) {
                // store epsilon roles seperately
                epsilon_rules.clear();
                epsilon_rules[rule.lhs] = rule.id;
            } else {
                for(vector<RuleToken>::iterator t = rule.rhs.begin(); t != rule.rhs.end(); t++) {
                    RuleToken tok = *t;
                    if (tok.is_terminal) {           
                        unordered_set<int> rules_for_terminal = terminal_to_rule_ids[tok.terminal];                             
                        rules_for_terminal.insert(rule.id);
                        terminal_to_rule_ids[tok.terminal] = rules_for_terminal;
                        found_terminal = true;
                    }
                }
                if (!found_terminal) 
                    nonterminal_rules.insert(rule.id); 
            }
        }
    };


}

