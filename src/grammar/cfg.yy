%{
#include "cfgrammar.h"
#include <string>
#include <vector>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <cmath>
#include "cfgparser.h"
using namespace std;


%}

%start grammar 

%defines
%skeleton "lalr1.cc"

%lex-param { cfgparser::CfGrammar* driver } 
%parse-param { cfgparser::CfGrammar* driver}

%initial-action {
    yyin = fopen(driver->filename.c_str(),"r");
}

%union { // this is awkward. The union cannot contain class types, only pointer types.
  char *string;
  std::string *token_val;
  cfgparser::RuleToken *token; 
  cfgparser::Rule *rule;
  vector<cfgparser::RuleToken> *tokenlist;
}

%type <tokenlist> rhs;
%type <rule> grammar;
%type <token> symbol;

%token ARROW
%token <token_val> TOKEN
%token <token> NONTERMINAL 
%token <token> TERMINAL
%token SEMICOLON
%token KLEENE
%token PLUS 
%token NTMARKER

%{
    extern int yylex(yy::parser::semantic_type *yylval, cfgparser::CfGrammar* driver ); // provide the yylex function to the C lexer
    extern FILE *yyin;

    namespace yy {

            void parser::error(const string& s) {
                cerr << "error: "<< s << endl;
            }
            //void parser::error(const location_type& l, const string& s) {
            //    cerr << "error: "<< s << endl;
            //}

          
    }

%}

%%

grammar:
      line {}
    | grammar line {}

line: 
    TOKEN ARROW rhs SEMICOLON TOKEN {driver->add_rule(new cfgparser::Rule(0, *$1, (*$3), std::log(std::stod(*$5))));} 
   | TOKEN ARROW SEMICOLON TOKEN {driver->add_rule(new cfgparser::Rule(0, *$1, *(new vector<cfgparser::RuleToken>()), std::log(std::stod(*$4))));} 

rhs:
     symbol {$$ = new vector<cfgparser::RuleToken>(); $$->push_back(* $1);} 
   | rhs symbol  {$$ = $1; $1->push_back(* $2);}
    
symbol: 
      TOKEN NTMARKER KLEENE {$$ = new cfgparser::RuleToken; $$->nonterminal=*$1; $$->is_terminal=false; $$->has_kleene=true;} 
    | TOKEN NTMARKER {$$ = new cfgparser::RuleToken; $$->nonterminal=*$1; $$->is_terminal=false; $$->has_kleene=false; }
    | TOKEN {$$ = new cfgparser::RuleToken; $$->terminal=*$1; $$->is_terminal=true; $$->has_kleene=false;}
%%

