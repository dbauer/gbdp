%{
#include "cfgrammar.h"
#include <string>
#include <iostream>
#include "cfgparser.h"
using namespace std;

typedef yy::parser::token tok;

#define YY_DECL int yylex(yy::parser::semantic_type *yylval, cfgparser::CfGrammar* driver)

%}
%option noyywrap
delim [ \t\n]
ws {delim}+
string [^ \t\*\$\n][^ \t;\*\$\n]* 
%%
{ws}    {}
\; {return tok::SEMICOLON;}
-> {return tok::ARROW; }
\* {return tok::KLEENE; }
\$ {return tok::NTMARKER; }
{string} {yylval->token_val = new std::string(yytext); return tok::TOKEN;} 
%%

