CC=g++
CFLAGS=-Wall -std=c++0x -c 
SRCDIR=src/
BIN=bin/

all: gbdp.o
	@mkdir -p bin
	$(CC) -g -std=c++0x  -o $(BIN)gbdp $(BIN)/*.o $(SRCDIR)/logprob.cc $(SRCDIR)/earley.cc $(SRCDIR)/main.cc 

gbdp.o:
	cd $(SRCDIR); $(CC) -c -std=c++0x -Wall -o $@ item.cc 
	@mkdir -p bin
	mv $(SRCDIR)/*.o bin

cfgparser:
	flex -o $(SRCDIR)/cfglexer.c $(SRCDIR)/grammar/cfg.l
	bison -y --defines=$(SRCDIR)/cfgparser.h -o $(SRCDIR)/cfgparser.cc $(SRCDIR)/grammar/cfg.yy
	cd $(SRCDIR); $(CC) -c -std=c++0x -Wall rule.cc cfglexer.c cfgparser.cc cfgrammar.cc
	@mkdir -p bin
	mv $(SRCDIR)/*.o bin

clean:
	rm -rf bin/
	rm -f $(SRCDIR)/cfgparser.h $(SRCDIR)/cfgparser.cc $(SRCDIR)/cfglexer.c $(SRCDIR)/location.hh $(SRCDIR)/position.hh $(SRCDIR)/stack.hh 
